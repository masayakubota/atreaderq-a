<!-- 新着記事表示 -->
<?php
$args = [
      'category_name' => 'サーバー系',
      
  ];
?>

<?php if(current_user_can('author') ):?>
<ul>
<?php
// 条件を渡して記事を取得
$custom_posts = get_posts($args);

foreach ( $custom_posts as $post ): setup_postdata($post); ?>
    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php endforeach; ?>
</ul>
<?php endif; ?>


<!-- 新着記事表示 -->