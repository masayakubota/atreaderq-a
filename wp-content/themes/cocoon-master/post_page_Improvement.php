<?php
$args = [
      'category_name' => '改善を提案する',
      
  ];
?>

<?php if(current_user_can('administrator') ):?>
<ul>
<?php
// 条件を渡して記事を取得l
$custom_posts = get_posts($args);

foreach ( $custom_posts as $post ): setup_postdata($post); 
$remove_array = ["\r\n", "\r", "\n", " ", "　"];
$content = wp_trim_words(strip_shortcodes(get_the_content()), 50, '…' );
$content = str_replace($remove_array, '', $content);?>
    <li><?php the_time('Y/m/d') ?><a href="<?php the_permalink($post); ?>"><?php echo $content; ?>
</a></li>
<?php endforeach; ?>
</ul>
<?php else : echo " 投稿者 "  ; ?>

<?php endif; ?>