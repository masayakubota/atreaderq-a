<?php
//ヘッダー部分（<head></head>内）にタグを挿入したいときは、このテンプレートに挿入（ヘッダーに挿入する解析タグなど）
//子テーマのカスタマイズ部分を最小限に抑えたい場合に有効なテンプレートとなります。
//例：<script type="text/javascript">解析コード</script>
?>
<?php if (!is_user_administrator()) :
//管理者を除外してカウントする場合は以下に挿入 ?>


<?php endif; ?>
<?php //全ての訪問者をカウントする場合は以下に挿入 ?>

<li><h2>ログイン</h2>
<?php if (is_user_logged_in()) : ?>
	Welcome! <?php global $current_user; echo $current_user->display_name ?>さん<br />
	ログアウトは &raquo; <a href="<?php echo wp_logout_url() ?>&amp;redirect_to=<?php echo esc_attr($_SERVER['REQUEST_URI']) ?>">こちら</a>
<?php else : ?>
	<form method="post" action="<?php echo wp_login_url() ?>?redirect_to=<?php echo esc_attr($_SERVER['REQUEST_URI']) ?>">
		<p><label for="login_username">ユーザー名：</label><br />
		<input type="text" name="log" id="login_username" value="" /></p>
		<p><label for="login_password">パスワード：</label><br />
		<input type="password" name="pwd" id="login_password" value="" /></p>
		<p><input type="submit" value="ログイン" /></p>
	</form>
<?php endif; ?>
</li>