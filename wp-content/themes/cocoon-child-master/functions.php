<?php //子テーマ用関数
if ( !defined( 'ABSPATH' ) ) exit;

//子テーマ用のビジュアルエディタースタイルを適用
add_editor_style();

//以下に子テーマ用の関数を書く

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

//phpの読み込み

function Include_my_php($params = array()) {
  extract(shortcode_atts(array(
      'file' => 'default'
  ), $params));
  ob_start();
  include(get_theme_root() . '/' . get_template() . "/$file.php");
  return ob_get_clean();
} 

add_shortcode('myphp', 'Include_my_php');




// // 購読者権限グループを削除
// remove_role( 'login_user' );

// // 寄稿者権限グループを削除
// remove_role( 'marketer' );

// // 投稿者権限グループを削除
// remove_role( 'author' );

// // 編集者権限グループを削除
// remove_role( 'login' );

// // 編集者権限グループを削除
// remove_role( 'Webmaster' );


add_role('login_user' , "ログインユーザー");

add_role('marketer' , "販社");
