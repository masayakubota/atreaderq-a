<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link https://ja.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'wp-test' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'root' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', 'root' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'localhost' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define( 'DB_COLLATE', '' );

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '5p97Hl||05DGP:SN$GTX=XlYPQQLtaO%FSVBMdc8UH)8ejc%1A|)X__~8G}C;W3t' );
define( 'SECURE_AUTH_KEY',  '.JQGa3cE~%;TpFISl*~DOwqS6h~ (|a{l+ @@?)!S}`S>2Kt1m^Rxk${vFasBmq5' );
define( 'LOGGED_IN_KEY',    'Qxu6?/j`02RBl|`0wEIjy]FMqN^s>uFNw6cMl/s.?H&t0#5902Ua134~Ay+8x7^f' );
define( 'NONCE_KEY',        'is{iLs``kV[+iMR<Px^Y)D}8ahhW`[F!7/Ab*kB|kVJ9xeZJZ0HCrz6G>aEC^}m<' );
define( 'AUTH_SALT',        '(1Aq*X<M-lgTQgU?@T%!@2!vZd/f;{_)#0x}5e+stO;rp`*UK!pQd$>KdrF+yTo~' );
define( 'SECURE_AUTH_SALT', 'pjCj*QIh?CQ*$?6>TEF]+!1e*26aRtk/C.y(6Q>5M>U7>F._ntr|>mUzZO=xeX4`' );
define( 'LOGGED_IN_SALT',   '2%0:yoB<1D:(NERhVFQxKKMYt)F[/7uj*qL.JgJTAO.WFDmbG<REJ](b2k<9ihN1' );
define( 'NONCE_SALT',       'E#sSDh@@89R{dSgA#qv@^Xm/7Q1S4gvic+A$52k8SjW3u}1;Vvag$D?7X|~Wh*iQ' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数についてはドキュメンテーションをご覧ください。
 *
 * @link https://ja.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

// 認証ありプロキシ
define('WP_PROXY_HOST', 'http://wordpress:wordpress@proxy.ysk.co.jp');
define('WP_PROXY_PORT', '80');

// 認証に関係なくこれも追加
// ここをFALSEにするとプロキシを使わないようできる
define('WP_USEPROXY', 'TRUE');

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

define('FS_METHOD', 'direct');